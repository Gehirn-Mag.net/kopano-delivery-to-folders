#!/usr/bin/perl -w

# 
# MIT License
# 
# Copyright (c) 2019 Steffen Schoch - mein@gehirn-mag.net
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


#
# Stellt Mails im öffentlichen Ordner zu.
# 2018-09-04, Schoch: Wechsel auf hosted Kopano
# 2013-03-##, StS: Erstellung
#

use strict;
use warnings;

use File::Basename;
use IPC::Open2;
use Net::LDAP;
use Unix::Syslog qw(:macros :subs);

# Auszug aus sendmail sysexit.h
use constant {
  EX_OK           => 0,
  EX_TEMPFAIL     => 75,
  EX_UNAVAILABLE  => 69,
  EX_DENIED       => 77
};

# LDAP-Einstellungen
my $ldapConf = {
  base     => 'ou=CharlieB.xxxxxx,dc=xxxxxx,dc=xxxxxx',
  dn       => 'cn=postfix,dc=xxxxxx,dc=xxxxxx',
  password => 'xxxxxx',
  debug    => 0
};

# Hier Mail hin senden falls kein Ordner gefunden wurde:
my $defaultMailTo = 'info@gehirn-mag.net';
# Pfad zu dagent (gobale Optionen möglich)
my $dagent = '/usr/sbin/kopano-dagent';

# Init
$ENV{PATH} = ''; # Taint Mode...
#openlog basename($0, '.pl'), LOG_PID | LOG_PERROR, LOG_MAIL;
openlog basename($0, '.pl'), LOG_PID, LOG_MAIL;

# LDAP verbinden (sollte immer funktionieren, sonst hat Postfix vorher schon ein
# Problem...)
my $ldap = Net::LDAP->new('127.0.0.1', debug => $ldapConf->{debug})
  or myExit(EX_TEMPFAIL, 'Can not connect to LDAP-Backend.' . $!
);
$ldap->start_tls('verify' => 'none') || myExit(EX_TEMPFAIL, 'LDAP TLS failed');
my $mesg = $ldap->bind('dn' => $ldapConf->{'dn'}, 'password' => $ldapConf->{'password'})
  || myExit(EX_TEMPFAIL, 'LDAP bind failed: ' . $!);
print $mesg->error;

# Argumente holen: Immer nur der Empfänger...
my $recipient = shift;
if(not defined $recipient) {
    syslog LOG_INFO, 'ERROR: no recpient given';
    myExit(EX_DENIED, 'no recipient given');
}

# Im LDAP suchen ob Empfaenger lokal ist (Dovecot: destinationIndicator=mail, Zarafa: zarafaAccount=1)
my $result = $ldap->search(
  base    => $ldapConf->{base},
  filter  => '(&(objectClass=sitsPostfixTransport)(PTransDest=kopanoPublicFolder:*)(PTransSrc=' . $recipient . '))',
  attrs   => [ 'PTransDest' ]
);
my $pFolder = undef;
foreach my $entry ($result->entries) {
  #y $dn = $entry->dn;
  if($entry->get_value('PTransDest') =~ m/^[^:]+:([^:]+)(:(.+))?/o) {
    if(defined $3) {
      # Company/Mail mit drin: "kopanoPublicFolder:info@gehirn-mag.net:Info"
      $pFolder = $3;
      $defaultMailTo = $1;
    } else {
      # "klassische" Form nur mit Ordner: "kopanoPublicFolder:Info"
      $pFolder = $1;
    }
  }
}
my $resultCode = EX_OK;
my $resultMsg = 'Ok';
if(not $pFolder) {
    # Kein Public Folder Eintrag gefunden, somit $defaultMailTo
    my $pid = open2(\*CHILDOUT, \*CHILDIN, $dagent . ' ' . $defaultMailTo);
    print CHILDIN while <>;
    close CHILDIN;
    my $dResult;
    $dResult .= $_ while <CHILDOUT>;
    close CHILDOUT;
    # waitpid($pid, 0) ist mir hier egal... Zombies, yeah!
    if($? != 0) {
        # dagent meldete Fehler zurück
        $resultCode = EX_TEMPFAIL;
        $resultMsg = 'dagent delivery problem: ' . $dResult;
    }
    # Log-Eintrag
    syslog LOG_INFO, 'no public folder found for %s. Delieverd to defaultMailTo %s: %s (%d).',
        $recipient,
        $defaultMailTo,
        $resultMsg,
        $resultCode;
    syslog LOG_INFO, 'dagent: %s', $dResult if $dResult;
} else {
    # Versuchen in Public-Folder zu zu stellen
    my $pid = open2(\*CHILDOUT, \*CHILDIN, $dagent . ' -p/ -C -P \'' . $pFolder . '\' '  . $defaultMailTo);
    print CHILDIN while <>;
    close CHILDIN;
    my $dResult;
    $dResult .= $_ while <CHILDOUT>;
    close CHILDOUT;
    # waitpid($pid, 0) ist mir hier egal... Zombies, yeah!
    if($? != 0) {
        # dagent meldete Fehler zurück
        $resultCode = EX_TEMPFAIL;
        $resultMsg = 'dagent delivery problem: ' . $dResult;
    }
    # Log-Eintrag
    syslog LOG_INFO, 'delivered mail for %s to public folder %s. %s (%d)',
        $recipient,
        $pFolder,
        $resultMsg,
        $resultCode;
    syslog LOG_INFO, 'dagent: %s', $dResult if $dResult;
}

# Fertig und weg...
$ldap->unbind if $ldap;
myExit($resultCode, $resultMsg);


##### Funktionen ###############################################################


# Setzt syslog-Eintrag und beendet dieses Skript.
sub myExit {
  my $exitCode = shift;
  $exitCode = 77 unless defined $exitCode;
  my $exitMessage = shift || 'someone didn\'t know how to use me...';

  # Syslog - ich logge eigentlich alles bereits oben...
  #syslog LOG_INFO, 'Exit %d: "%s"', $exitCode, $exitMessage;

  # aufräen und weg hier...
  print $exitMessage, "\n";
  exit $exitCode;
}

