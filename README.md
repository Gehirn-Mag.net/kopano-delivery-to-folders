# Kopano delivery to folders

Kopano scripts and ldap schema for delivering mails direkt to private or public folders.

---

Kopano is a great collaboration suite. But I was missing an easy way to direct deliver mails direct into a private or public folder out of the box managed via LDAP.

So I wrote two scripts a long time ago when Kopano was still known as Zarafa.

This is a simple step by step guide. Feel free to adjust all paths - but please remember to change them everywhere.

## Step 1: Copy the perl scripts to /usr/local/bin
Or to any place you want. Please check that the required perl modules are installed. Edit both scripts and set $ldapConf to your needs.

## Step 2: If you like extend your ldap schemas
I have added a sample ldap schema which could be used. If you use a other schema please check the ldap filter in both scripts.

The schema contains more elements than used here. Just leave them in or remove them. Details of what you can do will follow in a future blog post.

## Step 3: Extend postfix master.cf
```
# Kopano Private/Public-Folder
kopanoPrivateFolder	unix - n n - - pipe
    flags=DORhu user=kopano:kopano argv=/usr/local/bin/kopanoPrivateFolder.pl ${recipient}
kopanoPublicFolder	unix - n n - - pipe
    flags=DORhu user=kopano:kopano argv=/usr/local/bin/kopanoPublicFolder.pl ${recipient} 
```

## Step 4: A sample LDAP Entry
Could look like this:
```
dn: PTransSrc=info@gehirn-mag.net,ou=Postfix,dc=gehirn-mag,dc=net
objectClass: top
objectClass: sitsPostfixTransport
PActive: TRUE
PTransDest: kopanoPublicFolder:info@gehirn-mag.net:Gehirn-Mag.Net/Info
PTransSrc: info@gehirn-mag.net
```
My Kopano is setup in hosted mode. If you have a "normal" installation just remove the mail address in PTransDest that is looks like this: kopanoPublicFolder:Gehirn-Mag.Net/Info


---

More details will follow on my personal blog soon: [https://gehirn-mag.net/einmal-emails-in-kopano-unterordner-bitte/](https://gehirn-mag.net/einmal-emails-in-kopano-unterordner-bitte/).

---

Links to Kopano:

- Kopano: [https://kopano.com/](https://kopano.com/)
- Kopano community: [https://kopano.io/](https://kopano.io/)